import { useState, useEffect } from "react";
import "./App.css";
import Cards from "./components/Cards";
import Entrada from "./components/Entrada";
import GlobalStyle from "./styles/global";

//--------------------------------------------------------------------------------
function App() {
  //---------------------Estados--------------------------------------------------
  const [dados, setDados] = useState([]);
  const [owner, setOwner] = useState("vazio");
  const [repo, setRepo] = useState("");
  const [erro, setErro] = useState(false);

  //----------------------------funções-------------------------------------
  const handlePath = (path) => {
    setOwner(path[0]);
    setRepo(path[1]);
  };

  const verificandoExistencia = (response) => {
    let aux = dados.filter((elem) => elem.id === response.id);

    if (aux.length > 0) {
      console.log("o repositório já foi add");
    } else {
      setDados([...dados, response]);
    }
  };
  //---------------------UseEffect - montagem e atualização -------------------
  useEffect(() => {
    fetch(`https://api.github.com/repos/${owner}/${repo}`)
      .then((response) => {
        // console.log("primeiro then ", response);
        if (
          response.status === 404 &&
          (owner !== "vazio" || dados.length > 0)
        ) {
          setErro(true);
          setOwner("vazio");
        }
        return response.json();
      })
      .then((response) => {
        // console.log("segundo then ", response);
        if (!response.hasOwnProperty("message")) {
          verificandoExistencia(response);
          setErro(false);
        }
      })
      .catch((err) => console.log(err));
    // err.response.textStatus
  }, [owner, repo]);

  // console.log("dados", dados);
  // console.log("erro", erro);

  //-----------------------------------------------------------
  //-------------------------JSX
  //-----------------------------------------------------------
  return (
    <div className="App">
      <GlobalStyle />
      <header className="App-header">
        <Entrada handlePath={handlePath} erro={erro} />
        {dados && <Cards dados={dados} />}
      </header>
    </div>
  );
}

export default App;
