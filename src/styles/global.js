import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    body,div,header,p,:-ms-input-placeholder,button{
        margin: 0;
        padding: 0;
        text-decoration: none;
        outline: none;
        box-sizing: border-box;
    }
`;
export default GlobalStyle;
