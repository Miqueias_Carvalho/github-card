import { useState } from "react";
import { ContainerInput, ErroMessage } from "./styles";

function Entrada({ handlePath, erro }) {
  const [repos, setRepos] = useState("");

  const handleChange = (e) => {
    setRepos(e.target.value);
  };

  const handleClick = () => {
    let aux = repos.indexOf("/");
    let path = [];
    if (aux >= 0) {
      let owner = repos.slice(0, aux);
      let repo = repos.slice(aux + 1);
      path = [owner, repo];
    } else {
      path = [repos];
    }
    // console.log("path", path);
    handlePath(path);
  };

  //JSX--------------------------------------------------------------
  return (
    <ContainerInput>
      <div>
        <input
          type="text"
          placeholder="owner/repo"
          value={repos}
          onChange={handleChange}
        />
        <button onClick={handleClick}>Pesquisar</button>
      </div>

      <ErroMessage>{erro && <p>* Erro na busca do repositório</p>}</ErroMessage>
    </ContainerInput>
  );
}
export default Entrada;
