import styled from "styled-components";

export const ContainerInput = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  div input {
    font-size: 16px;
    height: 30px;
    width: 300px;
  }
  div button {
    height: 36px;
    width: 100px;
    font-size: 15px;
    background-color: #09669c;
    color: white;
    margin-left: 5px;
  }
`;

export const ErroMessage = styled.div`
  height: 40px;
  align-self: flex-start;
  p {
    font-size: 12px;
    color: #c76464;
  }
`;
