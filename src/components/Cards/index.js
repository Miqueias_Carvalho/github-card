import Card from "../Card";

function Cards({ dados }) {
  return (
    <>
      {dados.map((elemInfo) => (
        <Card key={elemInfo.id} info={elemInfo} />
      ))}
    </>
  );
}
export default Cards;
