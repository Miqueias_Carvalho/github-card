import styled from "styled-components";

export const ContainerRepo = styled.div`
  margin-top: 8px;
  display: grid;
  grid-gap: 5px;
  grid-template-columns: 100px 400px;
  grid-template-rows: 25px auto;
  grid-template-areas:
    "imagem titulo"
    "imagem descricao";

  align-items: center;
  justify-items: start;
  background-color: #314456;
  border-radius: 3px;
  padding: 10px;
`;

export const Imagem = styled.div`
  grid-area: imagem;
  width: 70px;
  justify-self: center;

  img {
    width: 100%;
  }
`;
export const Titulo = styled.p`
  grid-area: titulo;
  font-size: 20px;
  font-weight: 600;
`;
export const Descricao = styled.p`
  grid-area: descricao;
  font-size: 14px;
  text-align: left;
  color: antiquewhite;
`;
