import { ContainerRepo, Imagem, Titulo, Descricao } from "./styles";

function Card({ info: { owner, full_name, description } }) {
  return (
    <ContainerRepo>
      <Imagem>
        <img src={owner.avatar_url} alt="" />
      </Imagem>
      <Titulo>{full_name}</Titulo>
      <Descricao>{description}</Descricao>
    </ContainerRepo>
  );
}
export default Card;
